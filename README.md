# F1TV Live Program Schedule Calendar

F1TV don't provide a calendar link of their live broadcast. This script will return an iCal of all on track action and pre and post action live broadcasts. This is a docker build for self hosting and should be called via a cronjob. Alternatively you can subscribe here:

https://gitlab.com/Plebster/f1tv/-/raw/main/f1tv.ics?ref_type=heads

The schedule is often only updated during race week with F2 and F3 sometimes not being populated until the Thursday.

## Season updating

This should be automatic now. This is handled by this function if there are issues
    
    extract_season_id()

## build

    docker build -t f1tv .

## dev

    docker run --rm -it -v $PWD:/cal python:3 bash

    cd cal
    pip install --no-cache-dir -r requirements.txt
    python f1tv.py

## run

    docker run --rm -v $PWD:/cal f1tv
